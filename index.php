<?php

function theArray(array $array, int $n) {
    $result = [];
    if ($n > 0) {
        for ($i = 0; $i < $n; $i++) {
            $temp1 = 0;
            $temp2 = 0;
            $temp3 = 0;
            if (isset ($array[$i-1])) {
                $temp1 = $array[$i-1];
            }
            if (isset ($array[$i])) {
                $temp2 = $array[$i];
            }
            if (isset ($array[$i+1])) {
                $temp3 = $array[$i+1];
            }
            array_push($result, $temp1 + $temp2 + $temp3);
        }
    }
    
    return $result;
}

$a = [4,0,1,-2,3];
print_r(theArray($a, 5));